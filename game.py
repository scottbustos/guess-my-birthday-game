from random import randint

name = input("Hi! What is your name? ")


for guess_number in range(1, 6):
    random_month = randint(1, 12)
    random_year = randint(1950, 2022)

    # Computer guess
    print("Guess: ", guess_number, name, "were you born in?",
          random_month,  "/",  random_year)

    # Answer yes or no to the computers guess
    guess = input("yes or no? ")

    # Conditonal statment if the computer guessed right

    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I've got better things to do!")
    else:
        print("Drat! Lemme try again!")
